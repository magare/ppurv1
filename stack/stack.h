/*
 *  File name: stack.h
 *  Author: Sava Jakovljev
 *  Date: 9.11.2015.
 */

#ifndef STACK_H_INCLUDED
#define STACK_H_INCLUDED

#include <stdint.h>
#include <stdlib.h>

#define STACK_PUSH_COPY          1
#define STACK_PUSH_REFERENCE     2


struct stack;

/*
 * Function name:   stack_create
 * Parameters:      uint8_t element_size    -   size of one element stored in stack
 *                                          -   must be greater than 0
 *                  uint8_t push_type       -   determines a way in which data is pushed to a stack
 *                                          -   it can either be STACK_PUSH_COPY, in which case a copy 
 *                                              of an element is made and pushed in a stack
 *                                          -   if STACK_PUSH_REFERENCE, passed argument to stack_push() 
 *                                              function is pushed into a stack.
 * Return value:    stack*                  -   pointer to a newly created stack structure
 *                  NULL                    -   if error occurred
 * Description:     creates a new stack with slot size equal to element_size
 */
struct stack* stack_create(char* name, size_t element_size, uint8_t push_type);

/*
 * Function name:   stack_destroy
 * Parameters:      struct stack* stack     -   pointer to a stack to destroy
 * Return value:    void
 * Description:     destroys all elements contained in stack
 */
void stack_destroy(struct stack*);

/*
 * Function name:   stack_get_name
 * Parameters:      struct stack* stack     -   pointer to a stack which name is being wanted
 * Return value:    const chat const*       -   description name for a stack pointed by argument passed
 * Description:     returns the descriptive name of a stack
 */
const char const* stack_get_name(struct stack* stack);

/*
 * Function name:   stack_push
 * Parameters:      struct stack* stack     -   pointer to a stack in which data is being pushed into
 *                  void* data              -   pointer to a data block to be pushed into a stack. 
 *                                          -   data can be pushed either by copy or by reference, 
 *                                              depending on argument push_type passed to stack_create()
 * Return value:    uint8_t                 -   status of push operation
 *                                          -   EINVAL - if invalid arguments have been passed
 *                                          -   ENOMEM - if not enough memory for stack push operation
 *                                          -   0      - on success
 * Description:     pushed a data block to a stack. 
 */
uint8_t stack_push(struct stack* stack, void* data);

/*
 * Function name:   stack_pop
 * Parameters:      struct stack* stack     -   pointer to a stack from which data is being poped
 *                  void* data              -   pointer to a void chunk to write stack data into. 
 * Return value:    uint8_t                 -   status of push operation
 *                                          -   EINVAL - if invalid arguments have been passed
 *                                          -   0      - on success
 * Description:     gets an elements from a stack(LIFO queue). 
 */
uint8_t stack_pop(struct stack* stack, void* data);

/*
 * Function name:   stack_get_size
 * Parameters:      struct stack* stack     -   pointer to a stack which size is being queried
 * Return value:    int16_t                 -   size of a stack pointed by argument passed 
 *                                          -   -1 - if invalid arguments have been passed
 *                                          -   size of stack in data blocks on success
 * Description:     gets an elements from a stack(LIFO queue). 
 */
int16_t stack_get_size(struct stack* stack);

/*
 * Function name:   stack_dump
 * Parameters:      struct stack* stack             -   pointer to a stack to be examined
 *                  void (*dump_function)(void*)    -   function to be used for operation on every stack element 
 * Return value:    uint8_t                         -   status of push operation
 *                                                  -   EINVAL - if invalid arguments have been passed
 *                                                  -   0      - on success
 * Description:     for debugging purposes. This function will pass trough every
 *                  stack element, performing an operation on every data block that has been pushed.
 */
uint8_t stack_dump(struct stack* stack, void (*dump_function)(void*));

#endif
