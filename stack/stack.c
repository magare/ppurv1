/* Source file for generic C stack implementation.
 * Author: Sava Jakovljev

 * This source code is used for building a stack library.
 * Size of each stack node is determined by an argument passed to a stack during its 
 * initialization. Following code was written in respect to C99 coding standard.
 * This library isn't intended to be used in a multithreaded applications without extra
 * protection provided by client programmer itself. 
 *
 */

#include "stack.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

struct stack_element
{
    void* data;
    struct stack_element* next;
};

struct stack
{
    struct stack_element* head;
    uint16_t size;
    char* name;
    size_t element_size;
    uint8_t push_type;
};

struct stack* stack_create(char* name, size_t element_size, uint8_t push_type)
{
    if((push_type == STACK_PUSH_COPY && element_size <= 0) || 
       (push_type != STACK_PUSH_COPY && push_type != STACK_PUSH_REFERENCE))
    {
        return NULL;
    }

    struct stack* new_stack = (struct stack*)malloc(sizeof(struct stack));
    if(new_stack == NULL)
    {
        return NULL;
    }

    new_stack->head = NULL;
    new_stack->size = 0;
    new_stack->element_size = element_size;
    new_stack->name = name;
    new_stack->push_type = push_type;

    return new_stack;
}

void stack_destroy(struct stack* stack)
{
   if(stack == NULL)
   {
       return;
   }

   struct stack_element* stack_element;
   while(stack->head != NULL)
   {
       stack_element = stack->head->next;
       free(stack->head->data);
       free(stack->head);
       stack->head = stack_element;
   }

   stack->size = 0;
}

uint8_t stack_push(struct stack* stack, void* data)
{
    if(stack == NULL)
    {
        return EINVAL;
    }

    struct stack_element* new_element = (struct stack_element*)malloc(sizeof(struct stack_element));
    if(new_element == NULL)
    {
        return ENOMEM;
    }

    new_element->next = stack->head;
    stack->head = new_element; 
    stack->size++;

    if(stack->push_type == STACK_PUSH_COPY)
    {
        new_element->data = malloc(stack->element_size);
        if(new_element->data == NULL)
        {
            free(new_element);
            return ENOMEM;
        }
        memcpy(stack->head->data, data, stack->element_size);
    }
    else
    {
        new_element->data = data;
    }

    return 0;
}

uint8_t stack_pop(struct stack* stack, void* data)
{
    if(stack == NULL || stack->head == NULL)
    {
        return EINVAL;
    }

    struct stack_element* stack_element = stack->head;
    
    if(stack->push_type == STACK_PUSH_COPY)
    {
        memcpy(data, stack_element->data, stack->element_size);
        free(stack_element->data);
    }
    else
    {
        data = stack_element->data;
    }

    stack->head = stack->head->next;
    stack->size--; 
    free(stack_element);
    
    return 0;
}

uint8_t stack_dump(struct stack* stack, void (*dump_function)(void*))
{
    if(stack == NULL || dump_function == NULL)
    {
        return EINVAL;
    }

    struct stack_element* stack_element = stack->head;
    while(stack_element != NULL)
    {
        dump_function(stack_element->data);
        stack_element = stack_element->next;
    }

    return 0;
}

int16_t stack_get_size(struct stack* stack)
{
    return (stack == NULL) ? -1 : stack->size;
}

const char const* stack_get_name(struct stack* stack)
{
    return (stack == NULL) ? NULL : stack->name;
}
