#include "permute.h"
#include "stack.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

struct state
{
    char* to_permute;
    uint16_t place;
    uint16_t next_char;
};

static char* swtch(const char* const to_permute, int x, int y)
{
    char* new_string = strdup(to_permute);
    assert(new_string != NULL);

    new_string[x] = new_string[y];
    new_string[y] = to_permute[x];

    return new_string;
}

uint8_t print_all_permutations(const char* const string)
{ 
    if(string == NULL)
    {
        return EINVAL;
    }

    struct state element;
    uint16_t count = 0;
    uint16_t i = 0;
    uint8_t result;
    struct stack* stack = stack_create("string stack", sizeof(struct state), STACK_PUSH_COPY);
    if(stack == NULL)
    {
        return ENOMEM;
    }

    element.to_permute = strdup(string);
    element.place = 0;
    element.next_char = 0;
    result = stack_push(stack, (void*)&element);
    assert(result == 0);

    while(stack_get_size(stack) != 0)
    {
        result = stack_pop(stack, (void*)&element);
        //assert(result == 0);
        
        if(element.place == strlen(element.to_permute) - 1)
        {
            printf("Permutation number %d\nString: %s\n", ++count, element.to_permute);
        }
        
        for(i = element.place; i < strlen(element.to_permute); i++)
        {
            struct state new_element;

            new_element.place = element.place + 1;
            new_element.next_char = i;
            new_element.to_permute = swtch(element.to_permute, element.place, i);
            result = stack_push(stack, (void*)&new_element);
            if(result != 0)
            {
                result = ENOMEM;
                goto exit;
            }
        }

        free(element.to_permute);
    }
    
    result = 0;
exit:
    stack_destroy(stack);
    return result;
}
