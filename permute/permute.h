#ifndef PERMUTE_H_INCLUDED
#define PERMUTE_H_INCLUDED

#include <stdint.h>

/*
 * Function name:   print_all_permutations
 * Parameters:      const char* const string -  string to permute
 * Return value:    uint8_t                  -  status of operation
 *                                           -  EINVAL if invalid string passed
 *                                           -  ENOMEM if not enoguh memory for permutations
 *                                           -  0 on success
 * Description:     prints all permutations of a given string
 */
uint8_t print_all_permutations(const char* const string);

#endif
