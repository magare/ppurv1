/*
 * U ovom zadatku realizovan je funkcija koja prikazuje sve permutacije stringova koji se prosledjuju kao agrumenti programa.
 * Program se oslanja da dinamicki stek, koji je izveden kao dinamicka biblioteka koja se povezuje prilikom pokretanja programa.
 * Modul koji nalazi i prikazuje permutacije je izveden kao staticka biblioteka.
 * Usled ogranicene maksimalne dubine poziva funkcija, dinamicki softverski stek koristi se za realizaciju rekurzije.
 * Za prevodjenje programa je prvo potrebno dinamicki prevesti modul steka na osnovu fajlova stack.c i stack.h koji se nalaze u stack direktorijumu.
 * Kao rezultat dobija se libstack.so, koji je potreban za prevodjenje statickog modula libpermutations.a na osnovu fajlova permutations.c i permutations.h
 * Stringovi koji se permutuju se prosledjuju kao argumenti u komandnoj liniji.
 *
 *
 * Ime i prezime: Sava Jakovljev
 * Indeks: E2 52/2015
 */


#include "stack.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "permute.h"

struct stack* stack;
unsigned int count = 0;
int i = 0;

void dump_function(void* data)
{
    int* value = (int*)data;
    printf("Element number %d from stack: %d\n", ++count, *value);
}

int main(int argc, char** argv)
{
    if(argc < 2)
    {
        printf("Error: no string to permute. Exiting...\n");
        return EINVAL;
    }

    uint8_t i = 0;
    for(i = 1; i < argc; i++)
    {
        printf("---------------------------   PERMUTATION FOR STRING NO %d ---------------------------\n", i);
        printf("-- String:%s\n", argv[i]);
        printf("-- String length: %d\n", strlen(argv[i]));
        print_all_permutations(argv[i]);
    }

    return 0;
}
